import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import App from './app';

ReactDOM.render(
    <Router>
        <div>
            <Route render ={()=> < App />} path="/" />
        </div>
    </Router>, document.getElementById('root'));