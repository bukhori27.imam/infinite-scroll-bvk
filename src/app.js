import InfiniteScroll from "react-infinite-scroll-component";
import React, { useEffect, useState } from 'react';
import { UncontrolledCollapse, Button, CardBody, Card } from 'reactstrap';
import Accordion from './component/Accordion'
import imgdefault from './imgdefault.png';
import './style.css';

const style = {
  // height: 30,
  border: "1px solid green",
  margin: 6,
  padding: 8,
  display: 'flex'
};
const imgCat = {
  width:'100px',
  height:'100%'
}
const paddingLeft = {
  paddingLeft: 10,
}
const paddingTop = {
  paddingTop: 8,
}
function List() {
  const [page, setPage] = useState(1);
  const [appState, setAppState] = useState({
    user: null,
    limit: 10
  });
  useEffect(() => {
    setAppState({ loading: true });
    const apiUrl = `https://api.thecatapi.com/v1/breeds?limit=10&page=1`;
    fetch(apiUrl)
      .then((res) => res.json())
      .then((repos) => {
        setAppState({loading: false, user: repos});
      });
  }, [setAppState]);
  function fetchMoreData () {
    var pages = page + 1;
    setPage(pages);
    const apiUrl = `https://api.thecatapi.com/v1/breeds?limit=10&page=` + page;
    fetch(apiUrl)
      .then((res) => res.json())
      .then((repos) => {
        if (repos.length > 0) {
          var data = appState.user.concat(repos);
          setAppState({loading: false, user: data});
        }else {
          setAppState({loading: false, user: appState.user});
        }
      });
  }
  return (
    <div className='App'>
      <div className='container'>
        <h1>My Repositories</h1>
      </div>
      {(appState.user && appState.user.length > 0) &&
        <InfiniteScroll
          dataLength={appState.user.length}
          next={fetchMoreData}
          hasMore={true}
          loader={appState.loading && <h4>Loading...</h4>}
          >
          {appState.user.map(({ name, image, description, weight, id}, index) => {
            return (
              <div style={style} key={index}>
                {image && <img src={image.url ? image.url : imgdefault} style={imgCat}/> }
                {!image && <img src={ imgdefault} style={imgCat}/> }
                <div style={paddingLeft}>
                  <div>Name: {name}</div>
                  {weight && (
                    <>
                      <div style={paddingTop}>weight - Imperial: {weight.imperial}</div>
                      <div style={paddingTop}>weight - Metric: {weight.metric}</div>
                    </>
                    )
                  }
                  <Accordion content={description} />
                </div>
              </div>
            )
          })}
        </InfiniteScroll>
      }
    </div>
  );
}

export default List;
